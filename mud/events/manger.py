from .event import Event2

class MangerEvent(Event2):
    NAME = "manger"

    def perform(self):
        if not self.object.has_prop("mangeable"):
            self.add_prop("object-not-mangeable")
            return self.manger_failed()
        self.inform("manger")

    def manger_failed(self):
        self.fail()
        self.inform("manger.failed")
